#! /usr/bin/env python

# conda execute
# env:
#  - python >=3
#  - numpy
import numpy as np

class NeuralNetwork:

    def __init__( self ):
        #   Seeding the random generator
        np.random.seed(1)

        #   Initializing weights for layer 1
        self.w0     =   2 * np.random.random( ( 4, 3 ) ) - 1

        #   Initializing weights for layer 2
        self.w1     =   2 * np.random.random( ( 3, 1 ) ) - 1


    def __activation( self, x ):
        #   Sigmoid activation
        return 1 / ( 1 + np.exp( -x ) )

    def __activation_prime( self, x ):
        #   Derivative of sigmoid
        return  x * ( 1 - x )

    def __forward_propogation( self, x, weights ):
        #   passing inputs through a single layer of the neural network
        #   Dot product of inputs and corresponding weights
        return self.__activation( np.dot( x, weights ) )

    def __backward_propogation( self, x ):
        return 1

    def train( self, trainingSetInputs, trainingSetOutputs, numberOfIterations = 100000 ):

        #   Adding a bias
        ones                =   np.atleast_2d( np.ones( trainingSetInputs.shape[ 0 ] ) )
        trainingSetInputs   =   np.concatenate( ( ones.T, trainingSetInputs ), axis = 1 )

        for i in range( numberOfIterations ):

            #   Forward propogation
            #
            #   Forward propogating through layer 1
            self.layerOneFF  =   self.__forward_propogation( trainingSetInputs, self.w0 )
            #   Forward propogating through layer 2
            self.layerTwoFF  =   self.__forward_propogation( self.layerOneFF, self.w1 )

            #
            #

            #   Calculate Error
            #
            #   layer 2 error
            layer2_error    =   trainingSetOutputs - self.layerTwoFF

            #   layer 2 delta (gradient descent)
            layer2_delta    =   layer2_error * self.__activation_prime( self.layerTwoFF )

            #   layer 1 error
            layer1_error    =   np.dot( layer2_delta, self.w1.T )

            #   layer 1 delta (gradient descent)
            layer1_delta    =   layer1_error * self.__activation_prime( self.layerOneFF )

            #   adjusting weights
            self.w0     +=  np.dot( trainingSetInputs.T, layer1_delta )

            self.w1     +=  np.dot( self.layerOneFF.T, layer2_delta )


    def predict( self, input ):

        #   Adding a bias
        input = np.concatenate( ( np.array( [ np.ones(1) ] ).T, input ), axis = 1 )
        #   Forward propogation
        #
        #   Forward propogating through layer 1
        layerOneFF  =   self.__forward_propogation( input, self.w0 )
        #   Forward propogating through layer 2
        layerTwoFF  =   self.__forward_propogation( layerOneFF, self.w1 )

        return layerTwoFF

if __name__ == "__main__":

    neuralNet               =   NeuralNetwork()

    trainingSetInputs       =   np.array([
                                    [ 0, 0, 0 ],
                                    [ 1, 1, 1 ],
                                ])

    trainingSetOutputs      =   np.array([ [1], [0] ])

    numberOfIterations      =   100000

    trainingOutput          =   neuralNet.train( trainingSetInputs, trainingSetOutputs, numberOfIterations )
    print( neuralNet.layerTwoFF )
    testingSetInputs        =   np.array( [ [ 1, 0, 1 ] ] )

    print( neuralNet.predict( testingSetInputs ) )
