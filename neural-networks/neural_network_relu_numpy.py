#! /usr/bin/env python

# conda execute
# env:
#  - python >=3
#  - numpy
import numpy as np

class NeuralNetwork:


    #   Init function
    def __init__( self, layersDefinition ):
        #   Seeding the numpy random generator
        np.random.seed( 1 )

        #   Initializing weights
        self.synaptic_weights = []

        #   Handling layers
        #   Range of weights in layers (-1,1) => b = 1; a = -1

        #   Input and Hidden layer
        for i in range( 1, len( layersDefinition ) - 1 ):
            r = ( 1 - ( -1 ) ) * np.random.random( ( layersDefinition[ i - 1 ] + 1, layersDefinition[ i ] + 1 ) ) + ( -1 )
            self.synaptic_weights.append( r )

        #   Output layer
        r = ( 1 - ( -1 ) ) * np.random.random( ( layersDefinition[ i ] + 1, layersDefinition[ i + 1 ] ) ) + ( -1 )
        self.synaptic_weights.append( r )

    #   ReLU function
    def __activation( self, x ):
        return ( x > 0 ) * x

    #   d/dx ReLU function
    def __activation_prime( self, x ):
        return ( x > 0 )

    #   Training function
    def train( self, trainingSetInputs, trainingSetOutputs, learningRate = 0.2, numberOfIterations = 100000 ):
        #   Adding a column of 1s ( Bias units ) to the trainingSetInputs
        #   https://www.quora.com/What-is-bias-in-artificial-neural-network
        #
        #   Assuming that the training set input is atleast a 2D array
        ones                =   np.atleast_2d( np.ones( trainingSetInputs.shape[ 0 ] ) )
        trainingSetInputs   =   np.concatenate( ( ones.T, trainingSetInputs ), axis = 1 )

        #   Begin iterating!
        for i in range( numberOfIterations ):
            #   Randomly picking an input from the training set inputs
            #   Why? cuz we don't want the neural net to fall into a local minima
            k = np.random.randint( trainingSetInputs.shape[ 0 ] )
            a = [ trainingSetInputs[ k ] ]

            for w in range( len( self.synaptic_weights ) ):
                #   Multiplying weights with inputs
                inputsDotWeights    =   np.dot( a[ w ], self.synaptic_weights[ w ] )

                #   Passing inputsDotWeights through the activation function
                #   here, tanH
                activationOutput    =   self.__activation( inputsDotWeights )

                #   Holding the activation output along with the randomly selected input
                #   for ease of processing
                a.append( activationOutput )


            #   Calculating error
            error = trainingSetOutputs[ k ] - a[ -1 ]


            #   Calculating required adjustments
            deltas = [ error * self.__activation_prime( a[ -1 ] ) ]


            #   Applying adjustments
            #   We start with the layer before the output layer
            for l in range( len( a ) - 2, 0, -1 ):
                deltas.append( deltas[ -1 ].dot( self.synaptic_weights[ l ].T ) * self.__activation_prime( a[ l ] ) )


            # reverse
            # [level3(output)->level2(hidden)]  => [level2(hidden)->level3(output)]
            deltas.reverse()

            #   Begin Back propogation
            #   1. Multiply activation output for an input with the corresponding delta
            #       This gives us the gradient
            #   2. Subtract a percentage of the gradient from the weight
            for w in range( len( self.synaptic_weights ) ):
                layer = np.atleast_2d( a[ w ] )
                delta = np.atleast_2d( deltas[ w ] )

                #   Adjusting weights
                self.synaptic_weights[ w ] += learningRate * layer.T.dot( delta )

    #   Predict function
    def predict( self, testingSetInputs ):
        #   Adding a column of 1s
        a = np.concatenate( ( np.ones(1).T, np.array( testingSetInputs ) ) )

        for w in range( len( self.synaptic_weights ) ):
            a = self.__activation( np.dot( a, self.synaptic_weights[ w ] ) )

        return a


if __name__ == '__main__':

    nn = NeuralNetwork([3,3,3,3,2,1])
    X = np.array([ [ 1, 1, 0 ], [ 1, 0, 1 ], [ 1, 0, 0 ], [ 0, 0, 1 ] ])
    y = np.array([ 0, 0, 1, 1 ])
    nn.train(X, y, 0.2, 10000)
    for e in X:
        print(e,nn.predict(e))
    print("-----------------------------------")
    testingSetInputs = np.array( [ 1, 1, 1 ] )

    print( testingSetInputs, nn.predict( testingSetInputs ) )
