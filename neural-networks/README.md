Neural Networks
====================
[Activation Functions](https://en.wikipedia.org/wiki/Activation_function)

All of the below neural networks are pure NumPY implementations.

### Single Neuron. Sigmoid activation. 10,000 runs
>   neural_network_sigmoid_numpy.py
>
>   Testing an XOR truth table
>
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )   

    Initial random synaptic weights before training

    [[-0.16595599]

    [ 0.44064899]

    [-0.99977125]]

    Number of training iterations

    10000

    New precise synaptic weights after training

    [[  3.33066907e-16]

    [  3.33066907e-16]

    [ -5.27355937e-16]]

    Training set

    [ 1, 0, 0 ]

    Output

    [[ 0.5]]

Sigmoids saturate and kill gradients. A very undesirable property of the sigmoid neuron is that when the neuron's activation saturates at either tail of 0 or 1, the gradient at these regions is almost zero. Recall that during back-propagation, this (local) gradient will be multiplied to the gradient of this gate's output for the whole objective. Therefore, if the local gradient is very small, it will effectively “kill” the gradient and almost no signal will flow through the neuron to its weights and recursively to its data.

[Sigmoid Drawbacks](http://www.kdnuggets.com/2016/03/must-know-tips-deep-learning-part-2.html)


### Neural Network. 3 layers. sigmoid activation. 10,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an XOR truth table
>   NN architecture: [ 3, 2, 1 ]
>
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )   

    Initial random synaptic weights before training

    [array([[-0.16595599,  0.44064899, -0.99977125],
       [-0.39533485, -0.70648822, -0.81532281],
       [-0.62747958, -0.30887855, -0.20646505],
       [ 0.07763347, -0.16161097,  0.370439  ]]), array([[-0.5910955 ],
       [ 0.75623487],
       [-0.94522481]])]

    Number of iterations

    10000

    New random synaptic weights after training

    [array([[-0.21259   ,  1.04225693, -1.48816244],
       [ 0.75268643, -5.1157055 , -5.13658854],
       [-4.82238555, -4.67113586,  2.27639716],
       [ 0.03099945,  0.43999697, -0.11795219]]), array([[-3.29287756],
       [ 5.82314821],
       [-3.71581338]])]

    Testing set

    [1 0 0]

    Output

    [ 0.12046479]

This is interesting! I was expecting it to be a lot closer to 1, but it wasn't. Perhaps the network needs more training, or perhaps a different activation function, or maybe, the layer architecture isn't right. Let's start by changing the layer architecture, doesn't make sense to dismiss the activation function, Sigmoid is, after all, the golden child of activation functions.

### Neural Network. 3 layers. sigmoid activation. 10,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an XOR truth table
>   NN architecture: [ 3, 3, 1 ]
>   
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )

    Initial random synaptic weights before training

    [array([[-0.16595599,  0.44064899, -0.99977125, -0.39533485],
           [-0.70648822, -0.81532281, -0.62747958, -0.30887855],
           [-0.20646505,  0.07763347, -0.16161097,  0.370439  ],
           [-0.5910955 ,  0.75623487, -0.94522481,  0.34093502]]), array([[-0.1653904 ],
           [ 0.11737966],
           [-0.71922612],
           [-0.60379702]])]

    Number of iterations

    10000

    New random synaptic weights after training

    [array([[-0.30017489,  0.99676324, -1.22453807,  0.31274772],
           [-0.87855237, -4.3687185 , -4.5303725 ,  0.70888792],
           [-0.4561997 ,  4.73383717,  4.23048429,  0.59548006],
           [-0.7253144 ,  1.31234912, -1.16999164,  1.0490176 ]]), array([[-0.0463098 ],
           [ 5.4192097 ],
           [-5.9175169 ],
           [-2.80421594]])]

    Testing set

    [1 0 0]

    Output

    [ 0.12879391]

Hmm, an improvement of 0.008, that's .8%, that's absolutely insignificant. In this attempt, I have effectively increased the number of hidden layer nodes from 2 to 3. Next attempt, reducing the number of hidden layer nodes to 1

### Neural Network. 3 layers. sigmoid activation. 10,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an XOR truth table
>   NN architecture: [ 3, 1, 1 ]
>   
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )

    Initial random synaptic weights before training

    [array([[-0.16595599,  0.44064899],
           [-0.99977125, -0.39533485],
           [-0.70648822, -0.81532281],
           [-0.62747958, -0.30887855]]), array([[-0.20646505],
           [ 0.07763347]])]

    Number of iterations

    10000

    New random synaptic weights after training

    [array([[ 1.08315837,  2.35011673],
           [-4.95425162, -3.48099428],
           [-5.02732405, -3.4351407 ],
           [ 0.62163478,  1.6005892 ]]), array([[ 6.20499798],
           [-3.18629949]])]

    Testing set

    [1 0 0]

    Output

    [ 0.34280736]

The plot thickens! performance is up from 0.12 to 0.34, that's a 283% accuracy increase! Less nodes is definitely the way to go. Since I am already down to 1 node in the hidden layer, the next step is to eliminate the hidden layer all together.


### Neural Network. 3 layers. sigmoid activation. 10,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an XOR truth table
>   NN architecture: [ 3, 0, 1 ]
>   
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )

    Initial random synaptic weights before training

    [array([[-0.16595599],
       [ 0.44064899],
       [-0.99977125],
       [-0.39533485]]), array([[-0.70648822]])]

    Number of iterations

    10000

    New random synaptic weights after training

    [array([[-1.99956638],
       [ 4.54732006],
       [-4.94502374],
       [-2.22894525]]), array([[-3.52581565]])]

    Testing set

    [1 0 0]

    Output

    [ 0.03661763]

Arrghhhhhh. this is the worst configuration, throws my hypothesis of less layers out the window. The optimum configuration seems to be [ 3, 1, 1 ]. But still no where near the accuracy I need. On to my next suspect, more training! Increasing the number of iterations from 10,000 to 100,000 on my star configuration [ 3, 1, 1 ]


### Neural Network. 3 layers. sigmoid activation. 100,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an XOR truth table
>   NN architecture: [ 3, 1, 1 ]
>   
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )

    Initial random synaptic weights before training

    [array([[-0.16595599,  0.44064899],
           [-0.99977125, -0.39533485],
           [-0.70648822, -0.81532281],
           [-0.62747958, -0.30887855]]), array([[-0.20646505],
           [ 0.07763347]])]

    Number of iterations

    100000

    New random synaptic weights after training

    [array([[ 1.60511605,  4.85363545],
           [-6.37740546, -7.94827259],
           [-6.38541941, -7.96175067],
           [ 1.14359246,  4.10410792]]), array([[ 10.42843222],
           [ -5.71006597]])]

    Testing set

    [1 0 0]

    Output

    [ 0.46010574]

Whoooot! performance up from 0.34 to 0.46. 135% accuracy increase. Let's push this baby to 1 million iterations! H-u-l-k!!!


### Neural Network. 3 layers. sigmoid activation. 1,000,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an XOR truth table
>   NN architecture: [ 3, 1, 1 ]
>   
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )

    Initial random synaptic weights before training

    [array([[-0.16595599,  0.44064899],
           [-0.99977125, -0.39533485],
           [-0.70648822, -0.81532281],
           [-0.62747958, -0.30887855]]), array([[-0.20646505],
           [ 0.07763347]])]

    Number of iterations

    1000000

    New random synaptic weights after training

    [array([[  0.9293127 ,   8.11139884],
           [ -1.53235953, -10.37019703],
           [ -1.52987807, -10.37028183],
           [  0.46778911,   7.36187131]]), array([[ 25.66094843],
           [-16.50278369]])]

    Testing set

    [1 0 0]

    Output

    [ 0.99945483]

There it is! my holy grail! 0.999, as perfectly close as it gets. Looks like the network just needed more training. After a million (roughly) iterations, the network has learnt the xor gate. However, the first example, single neuron - 10000 runs reached 50% accuracy in 1/100th of the training. Let's see if that network is capable of achieving the same, or more, accuracy when trained a million times!

### Single Neuron. Sigmoid activation. 1,000,000 runs
>   neural_network_sigmoid_numpy.py
>
>   Testing an XOR truth table
>
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )   

    Initial random synaptic weights before training

    [[-0.16595599]
     [ 0.44064899]
     [-0.99977125]]

    Number of training iterations

    1000000

    New precise synaptic weights after training

    [[  3.33066907e-16]
     [  3.33066907e-16]
     [ -5.27355937e-16]]

    Training set

    [ 1, 0, 0 ]

    Output

    [[ 0.5]]

Nope. Looks like the single neuron has run into a dead end. Perhaps caught in a local minima, or has run into dead weights. Looking at the weights it has conjured, they are unimaginably tiny! exp-16 ! that's atomic! So the single neuron was no match to a Neural Network. The neural network achieved 99% accuracy in a million (rough) test runs. The winning combination is:

    Neural Network Architecture:

    3 input nodes, 1 hidden node, 1 output node

    Training:

    1 million iterations

Let's try the winning combination on a different data set, an AND gate. This will be tricky, as the only time the Neural Network will see a 1 is when all the inputs are 1, that is less than 13% of the times. Predicting a one will be even trickier

### Neural Network. 3 layers. sigmoid activation. 1,000,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an AND truth table
>   NN architecture: [ 3, 1, 1 ]
>   
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )


    Initial random synaptic weights before training

    [array([[-0.16595599,  0.44064899],
       [-0.99977125, -0.39533485],
       [-0.70648822, -0.81532281],
       [-0.62747958, -0.30887855]]), array([[-0.20646505],
       [ 0.07763347]])]

    Number of iterations

    1000000

    New random synaptic weights after training

    [array([[  0.54337678,   5.76338273],
       [ -7.8853119 , -10.23410058],
       [ -3.45706665, -10.32149882],
       [  0.08185319,   5.0138552 ]]), array([[-3.08798763],
       [-8.62152215]])]

    Testing set

    [1 0 0]

    Output while predicting a 0

    [ 0.47514397]

    Testing set

    [1 1 1]

    Output while predicting a 1

    [ 0.49986076]

Shambolic, to be critical. Despite the near impossibility of a 1, the neural network could predict a 1 with a certainty of almost 50%, however, it was unable to precisely predict a 0, which is what reduces confidence in the network. Let's try a more forgiving table. and XNOR gate.

### Neural Network. 3 layers. sigmoid activation. 1,000,000 runs
>   neural_network_sigmoid_numpy_multi_layer.py
>
>   Testing an XNOR truth table
>   NN architecture: [ 3, 1, 1 ]
>   
>   f(x)    = 1 / ( 1 + exp( -x ) )
>   f'(x)   = x * ( 1 - x )

    Initial random synaptic weights before training

    [array([[-0.16595599,  0.44064899],
           [-0.99977125, -0.39533485],
           [-0.70648822, -0.81532281],
           [-0.62747958, -0.30887855]]), array([[-0.20646505],
           [ 0.07763347]])]

    Number of iterations

    1000000

    New random synaptic weights after training

    [array([[  0.92933511,   8.1112981 ],
           [ -1.53234398, -10.37007833],
           [ -1.52987062, -10.37011665],
           [  0.46781152,   7.36177056]]), array([[-25.67704252],
           [ 16.51309508]])]

    Testing set

    [0 0 1]

    Output while predicting a 0

    [ 0.0167415]

    Testing set

    [1 1 0]

    Output while predicting a 1

    [ 0.06179062]

This is so strange. the performance just got a whole lot worse. This neural network was able to predict only the XOR gate perfectly. My next suspect is the algorithm itself! :( Let's see if Tensorflow can improve results.
