Neural Networks - TensorFlow
====================
1.  [TensorFlow](https://www.tensorflow.org/)

2.  [Why TensorFlow?](https://indico.io/blog/the-good-bad-ugly-of-tensorflow/)

At this time, there seems to be a divided opinion between Theano and Tensorflow. Where Theano is the grandaddy of machine learning libraries, Tensorflow is a new star with a lot of promise and potential. It's undeniable that Theano has been around longer than Tensorflow ( Theano started in 2007, Tensorflow in 2014), they both have their interests in being the best library in machine learning. With that in mind, it is safe to assume that both of these are good. Both of them offer distributed processing over CPUs and GPUs, both of them have the same, if not insignificantly different, ways of building/deploying a neural network. For some months, Tensorflow is faster than Theano, then Theano comes up with an update and improves performance. Theano pushes Tensorflow too in the same way as Tensorflow pushes Theano. I will be exploring both of these, for now, I'm starting with Tensorflow.

Unlike most of my contemporaries, I'm gonna do all of my development in

1.  Conda (IDE)
2.  ITerm2 (Terminal)
3.  Atom (Code editor)

I also, occasionally use Jupyter notebooks, but the above 1,2,3 give me a very "hackerman" feel :P

Before I begin development, I have to install tensorflow on my machine. and perhaps a few other stuff, idk, let's see how it goes.

### Installing Tensorflow

I am using Conda, as already mentioned, so I am gonna download tensorflow via Conda. I am going to download tensorflow for the default 'root' environment of Conda. [Conda Environments](https://conda.io/docs/using/envs.html) are a nifty way of setting up a virtual environment, capable of executing python scripts along with the necessary dependencies. I tend to install all dependencies, like Tensorflow, in my Conda root. That way, I can keep adding more dependencies as and when i require and run my machine learning code without having to see the !@#%$!@# irritating 'conda.exceptions.NoPackagesFoundError' message. The only thing I hate more than traffic is dependency errors.

Tensor flow is available on conda. Run the following command in terminal

    $   conda install -c conda-forge tensorflow

The download messages start popping up

>    Fetching package metadata .........
>    Solving package specifications: ..........
>
>    Package plan for installation in environment /Users/Nikhil/anaconda:
>
>    The following packages will be downloaded:
>
>        package                    |            build
>        ---------------------------|-----------------
>        protobuf-3.1.0             |           py35_0         6.8 MB  conda-forge
>        pbr-1.10.0                 |           py35_0          99 KB  conda-forge
>        mock-2.0.0                 |           py35_0         101 KB  conda-forge
>        tensorflow-0.12.1          |           py35_1        32.0 MB  conda-forge
>        ------------------------------------------------------------
>                                               Total:        39.0 MB
>
>    The following NEW packages will be INSTALLED:
>
>        mock:       2.0.0-py35_0  conda-forge
>        pbr:        1.10.0-py35_0 conda-forge
>        protobuf:   3.1.0-py35_0  conda-forge
>        tensorflow: 0.12.1-py35_1 conda-forge
>
>    Proceed ([y]/n)? y
>
>    Fetching packages ...
>    protobuf-3.1.0 100% |###############################################################################| Time: 0:00:08 823.84 kB/s
>
>    pbr-1.10.0-py3 100% |###############################################################################| Time: 0:00:00 163.69 kB/s
>
>    mock-2.0.0-py3 100% |###############################################################################| Time: 0:00:01  91.75 kB/s
>
>    tensorflow-0.1 100% |###############################################################################| Time: 0:00:50 660.62 kB/s
>
>    Extracting packages ...
>
>    [      COMPLETE      ]|##################################################################################################| 100%
>
>    Linking packages ...
>
>    [      COMPLETE      ]|##################################################################################################| 100%

That's it. That should install Tensorflow in Conda, ready to be worked up on. To check if it works, launch the Conda root environment

    $   source activate root

Now run the file, **tensorflow_test.py** using the following command

    $   python3 tensorflow_test.py

You should see the output,

>   42

Works!
