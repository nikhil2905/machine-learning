#! /usr/bin/env python

# conda execute
# env:
#  - python >=3
#  - numpy

from numpy import array, random, exp, dot



#   Declaring the NeuralNetwork class
class NeuralNetwork():

    def __init__( self ):
        #   Seeding the random number generator
        #   Random generators need a previous state, during its initialization, there is no previous state
        #   So we seed it to a previous state
        random.seed( 1 )

        #   Weights are called synaptic because the "synapses" of the neuron are weighted
        #   This Neural Network is 1 layer deep, with 3 inputs and 1 output
        #   Assigning initial random weights to the inputs of the Neural Network
        #   Initial random weights are always between the values -1, 0, 1 so as to cover all possible integers
        #   and have a mean of 0

        #   numpy.random = 3 x 1 array of random numbers from [ -1, 0 ]
        self.synaptic_weights = 2 * random.random( ( 3, 1 ) ) - 1


    #   Defining the sigmoid function
    def __sigmoid( self, x ):
        #   Equation of the sigmoid function
        return 1 / ( 1 + exp( -x ) )


    #   Defining the gradient of the sigmoid function
    #   It is the derivative of sigmoid function equation
    #   It indicates how confident we are about the existing weights
    #   The less confident we are, the more we change them
    def __sigmoid_derivative( self, x ):
        #   Refer Wolfram Math for more details about how the derivative is obtained
        return  x * ( 1 - x )


    #   Train the neural network
    #   The training process is one of trial and error,
    #   adjusting the synaptic weights at each step
    def train( self, trainingSetInputs, trainingSetOutputs, numberOfIterations ):
        #   Iterating for number of iterations specified
        for interation in range( numberOfIterations ):

            #   Pass the training set through our neural network (1 layer single neuron)
            output = self.think( trainingSetInputs )

            #   Calculate the error
            #   (difference between desired output and predicted output)
            error = trainingSetOutputs - output

            #   Multiply the error with the input and again with the gradient of the sigmoid curve
            #   Thus,
            #   less confident weights are adjusted more
            #   inputs which are zero don't affect the weights
            #   Dot product of 2 arrays
            adjustment = dot( trainingSetInputs.T, error * self.__sigmoid_derivative( output ) )

            #   Adjust the weights
            self.synaptic_weights += adjustment


    #   The neural network thinks
    #   This function declares how confident the neural network is about the weights it has applied
    def think( self, inputs ):
        #   Pass inputs through the neural network (1 layer single neuron)
        return self.__sigmoid( dot( inputs, self.synaptic_weights ) )


#   The below statement is used when
#   you want a certain code snippet to run only if it is directly addressed form the CLI
#   The below statement ensures that all code under it can't be reused in another file
if __name__ == "__main__":

    #   Initializing a single neural network
    neuralNetwork   =   NeuralNetwork()

    #   These are the weights applied to inputs
    print ("Initial random synaptic weights before training: ")
    print ( neuralNetwork.synaptic_weights )

    #   Defining the training set
    #   We have 4 inputs with 3 input values and 1 output value
    #   ( Python doesn't have native arrays, that's why we use numpy )
    trainingSetInputs   =   array( [ [0, 0, 1], [1, 1, 1], [1, 0, 1], [0, 1, 1] ] )

    #   We are transposing the output - Matrix arithmetic
    trainingSetOutputs  =   array( [ [1, 1, 0, 0] ] ).T

    #   Train the neural network using the training set
    #   Iterate 10K times, with small but precise adjustments each time
    #   Could easily be 100K times
    numberOfIterations = 1000000
    neuralNetwork.train( trainingSetInputs, trainingSetOutputs, numberOfIterations )

    print ("Number of training iterations: ")
    print ( numberOfIterations )

    print ("New precise synaptic weights after training: ")
    print ( neuralNetwork.synaptic_weights )

    #   Test the neural network
    #   Test set with 1 input, with 3 input values; output to be predicted
    testingSetInputs    =   array( [ [1, 0, 0] ] )

    print ("Training set [ 1, 0, 0 ]")
    print ("Output: ")
    print ( neuralNetwork.think( testingSetInputs ) )
