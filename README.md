# Machine Learning
### A result oriented dive into machine learning

This project stems from my personal need to use machine learning to do awesome stuff. Being very frank, I don't know what I'm looking for, all I know is machine learning is going to be, if it isn't already, the greatest support humanity has towards its pursuit of self driven evolution.

This repo is built with Python. I am a web developer, after PHP and Ruby-on-rails, Python was my language of choice as it has the lower learning gradient, compared to R, for a web developer to transition from web development to date science. Perhaps I will do R later!?!?!


### Neural Networks

Why am I beginning with neural networks? because they are the most talked about machine learning algorithms as of February 2017. I want to see what the fuss is all about. All of the below are pure numpy implementations. Navigate to the folder

    neural-networks

to read my in-depth research about various configurations of neural networks, different activation functions, cost functions, training lengths, etc. Following are my key notes.

1.  There are a few terms that you should know right after you read the definition of a neural network, they are,

    1.  **Input** is what you pass through the network, hoping to find an output. You can have 1 input or several inputs (preferably several, cuz a neural input to study 1 input is called a CALCULATOR!). In machine learning lingo, inputs are also called as features.

    2.  **Output** is what the neural network produces after processing the input. Our aim, while building a neural network, is to try to make this output as close to the desired output as possible.

    3.  **Iterations** number of times the neural network adjusts its weights (trains itself), in order to produce an output as close to the desired output as possible.

    4.  **Weights** are multipliers applied to each input when they are passing through the neural network. Influential inputs are assigned higher weights and non influential inputs are assigned lower weights. Influence of inputs is understood by the network through the numerous iterations it undergoes while the neural network is being trained.

    5.  **Training Set** is a set of inputs that you pass through the neural network, in order to find the most optimum combination of weights such that the output, is as close to the desired output as possible.

    6.  **Activation Function** is the function inside the neural network that defines if an input * weight combination is allowed to be passed through the network. There are many types of activation functions, most popularly, sigmoid, ReLU (Rectified Linear Units) and tanH.
        [Activation functions](https://en.wikipedia.org/wiki/Activation_function)

    7.  **Cost Functions / Error Functions** are used to calculate the error between the desired output and the output produced by the neural network. There are many types of error functions like Sum of squared errors, simple difference error, etc.
        [Cost Functions/Error Functions](http://stats.stackexchange.com/questions/154879/a-list-of-cost-functions-used-in-neural-networks-alongside-applications)

    8.  **Gradient Descent** is the technique used to train the neural network. After calculating the error, it is the gradient descent that determines how the weights have to be tweaked.

    9.  **Forward Propagation** is when inputs are passed through the entire network to produce an output. This is where the inputs are passed through each node of the neural network, are multiplied by their weights and passed through the activation function.

    10. **Backward Propagation** is when the error between the produced output and desired output is calculated and rectified to a certain extent via the gradient descent. This is the stage where the Cost function / Error function is applied on the output to produce a delta ( amount to change the weights by ).

    11. **Feed forward** means signals pass from input to output, without cycling back to the input. There is no feedback to the input if backward propagation is not explicitly established.

2.  To design a neural network, there are a few rules of thumb,

    1.    | Layer | Number of nodes |
        | ----- | --------------- |
        | Input Layer | Number of inputs / features |
        | Hidden Layer | ( Number of inputs + number of outputs ) / 2 |
        | Output Layer | **classification** number of labels, **regression** 1 |


3.  2 layer [ Input layer, Output layer ] neural networks are good for linearly seperable data, but you need to add hidden layers, to be able to handle linearly inseperable data. [2 layer vs 3 layer neural networks](http://iamtrask.github.io/2015/07/12/basic-python-network/)  
